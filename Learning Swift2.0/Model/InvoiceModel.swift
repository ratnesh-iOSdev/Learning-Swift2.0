//
//  InvoiceModel.swift
//  Learning Swift2.0
//
//  Created by Ratnesh Shukla on 09/07/15.
//  Copyright © 2015 Ratnesh Shukla. All rights reserved.
//

import UIKit


class InvoiceModel: NSObject {
    
    var invoiceNo: Int
    var totalPrice: Float
    var thisInvoiceItemsArray = [Items]()
    
    init(invoiceNo: Int, totalPrice: Float, thisInvoiceItemsArray: Items) {
        self.invoiceNo = invoiceNo
        self.totalPrice = totalPrice
        self.thisInvoiceItemsArray = [thisInvoiceItemsArray]
    }
}
