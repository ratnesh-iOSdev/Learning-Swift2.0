//
//  Items.swift
//  Learning Swift2.0
//
//  Created by Ratnesh Shukla on 09/07/15.
//  Copyright © 2015 Ratnesh Shukla. All rights reserved.
//

import UIKit

class Items: NSObject {
    
    var itemName: String
    var itemQuantity: Int
    var itemPrice: Float
    
    init(itemName: String, itemQuantity: Int, itemPrice:Float) {
        self.itemName = itemName
        self.itemQuantity = itemQuantity
        self.itemPrice = itemPrice
    }
}
