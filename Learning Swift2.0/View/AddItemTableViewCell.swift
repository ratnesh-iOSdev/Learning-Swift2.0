//
//  AddItemTableViewCell.swift
//  Learning Swift2.0
//
//  Created by Ratnesh Shukla on 10/07/15.
//  Copyright © 2015 Ratnesh Shukla. All rights reserved.
//

import UIKit

class AddItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var saveButton: UIButton!

    @IBOutlet weak var nameTF: UITextField!
   
    @IBOutlet weak var QunatityTF: UITextField!
    
    @IBOutlet weak var PriceTF: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
