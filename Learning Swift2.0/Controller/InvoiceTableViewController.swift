//
//  InvoiceTableViewController.swift
//  Learning Swift2.0
//
//  Created by Ratnesh Shukla on 15/07/15.
//  Copyright © 2015 Ratnesh Shukla. All rights reserved.
//

import UIKit

class InvoiceTableViewController: UITableViewController, UpdateInvoiceArray {

    let invoiceObjetcArray = [InvoiceModel]()
    
    var invoiceObjectDict = [String: [Items]]()

    var invoiceObjectKey:String = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    //  DELEGATE
    
    func addOrEditInvoiceArray(lItems: [Items], Key:String) {
        if(lItems.count > 0) {
            invoiceObjectDict["\(Key)"] = lItems
            tableView.reloadData()
        }
    }
    
    
    @IBAction func addButtonTappedToAddInvoice(sender: AnyObject) {
        self.performSegueWithIdentifier("SEGUE_INVOICE_TO_ITEM_LIST", sender: sender)
    }
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invoiceObjectDict.count;
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("INVOICE_LIST_CELL", forIndexPath: indexPath)
        if (invoiceObjectDict.count > 0){
            cell.textLabel?.text = "InvoiceNo: \(indexPath.row)"
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell: UITableViewCell? = self.tableView.dequeueReusableCellWithIdentifier("INVOICE_LIST_CELL")
        invoiceObjectKey = "\(indexPath.row)"
        self.performSegueWithIdentifier("SEGUE_INVOICE_TO_ITEM_LIST", sender: cell)
    }



    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SEGUE_INVOICE_TO_ITEM_LIST" {
            if let destination = segue.destinationViewController as? ItemListViewController {
                
                destination.delegateToAddUpdateInvoicePage = self
               
                if let cell = sender as? UITableViewCell {

                    destination.invoiceObjectKey = invoiceObjectKey
                    
                    destination.itemObjectArray = invoiceObjectDict["\(invoiceObjectKey)"]!
                    
                    print("Cell Clicked \(destination) \(cell)", appendNewline: false)
                }
                else if let button = sender as? UIBarButtonItem {
                   
                    print(button, appendNewline: false)

                    destination.invoiceObjectKey = "\(self.invoiceObjectDict.count)"
                }
            }
        }
    }

}
