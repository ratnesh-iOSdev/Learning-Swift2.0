//
//  AddOrEditViewController.swift
//  Learning Swift2.0
//
//  Created by Ratnesh Shukla on 09/07/15.
//  Copyright © 2015 Ratnesh Shukla. All rights reserved.
//

import UIKit

protocol UpdateItemsArrayProtocol {
    func addOrEditThisItem(lItem: Items)
}


class AddOrEditViewController: UIViewController, UITextFieldDelegate {
    
    var delegateToAddItem:UpdateItemsArrayProtocol?

    @IBOutlet weak var itemNameTF: UITextField!
    
    @IBOutlet weak var itemCountTF: UITextField!
    
    @IBOutlet weak var itemPriceTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func doneButtonTapped(sender: AnyObject) {
        
        let item: Items = Items(itemName: itemNameTF.text!, itemQuantity: Int(itemCountTF.text!)!, itemPrice: Float(itemPriceTF.text!)!)

        delegateToAddItem?.addOrEditThisItem(item)
        
        self.navigationController?.popViewControllerAnimated(true)
    }
}
