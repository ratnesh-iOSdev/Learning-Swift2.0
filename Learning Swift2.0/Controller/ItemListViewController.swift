//
//  ItemListViewController.swift
//  Learning Swift2.0
//
//  Created by Ratnesh Shukla on 15/07/15.
//  Copyright © 2015 Ratnesh Shukla. All rights reserved.
//

import UIKit

protocol UpdateInvoiceArray {
    func addOrEditInvoiceArray(lItems:[Items], Key:String)
}


class ItemListViewController: UITableViewController, UpdateItemsArrayProtocol {
    
    var itemObjectArray = [Items]()
    
    var invoiceObjectKey:String = ""
    
    var itemObject: Items = Items(itemName:"", itemQuantity: 0, itemPrice: 0.0)

    
    var delegateToAddUpdateInvoicePage:UpdateInvoiceArray?
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func doneButtonTappedToUpdateInvoicePage(sender: AnyObject) {
        self.delegateToAddUpdateInvoicePage?.addOrEditInvoiceArray(itemObjectArray, Key:invoiceObjectKey)

        self.navigationController?.popViewControllerAnimated(true)
    }
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func addButtonTappedToAddItem(sender: AnyObject) {
        self.performSegueWithIdentifier("SEGUE_ITEM_LIST_TO_ITEM", sender: sender)
    }

    func addOrEditThisItem (lItem: Items) {

        itemObjectArray.append(lItem)

        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemObjectArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ITEM_LIST_CELL", forIndexPath: indexPath)
        if (itemObjectArray.count > 0){
            let item: Items = itemObjectArray[indexPath.row]
            cell.textLabel?.text = "ItemName: \(item.itemName)  \t   Item Count: \(item.itemQuantity)"
            
            cell.detailTextLabel?.text = "Item Price: \(item.itemPrice)"
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell: UITableViewCell? = self.tableView.dequeueReusableCellWithIdentifier("ITEM_LIST_CELL")
        itemObject = itemObjectArray[indexPath.row]
        self.performSegueWithIdentifier("SEGUE_ITEM_LIST_TO_ITEM", sender: cell)
    }


    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SEGUE_ITEM_LIST_TO_ITEM" {
            if let destination = segue.destinationViewController as? AddOrEditViewController {
                destination.delegateToAddItem = self
                if let cell = sender as? UITableViewCell {
                    
                    destination.itemNameTF.text = itemObject.itemName
                    //destination.itemPriceTF.text = itemObject.itemPrice
                    //destination.itemCountTF.text = itemObject.itemQuantity
                    
                    print(" \(destination) \(cell)", appendNewline: false)
                }
                else if let button = sender as? UIBarButtonItem {
                    print(button, appendNewline: false)
                }
            }
        }
    }
}
